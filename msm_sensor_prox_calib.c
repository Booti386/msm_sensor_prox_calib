#include <errno.h>
#include <limits.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>
#include <unistd.h>

#include <sys/inotify.h>


char *str_concat(const char *s1, ...)
{
	va_list ap;
	const char *s;
	size_t size = 0;
	char *dst;
	char *d;

	va_start(ap, s1);

	s = s1;

	while (s)
	{
		size += strlen(s);

		s = va_arg(ap, const char *);
	}

	va_end(ap);

	dst = malloc(size + 1);
	if (!dst)
		return NULL;

	d = dst;

	va_start(ap, s1);

	s = s1;

	while (s)
	{
		size_t len;

		len = strlen(s);
		memcpy(d, s, len);
		d += len;

		s = va_arg(ap, const char *);
	}

	va_end(ap);

	*d = '\0';

	return dst;
}

static char *get_prox_en_filename(void)
{
	FILE *fp;
	char line[1024];
	char *fname = NULL;

	fp = fopen("/proc/bus/input/devices", "r");
	if (!fp)
		return NULL;

	while (!feof(fp))
	{
		fgets(line, sizeof(line), fp);
		if (strcmp(line, "N: Name=\"proximity_sensor\"\n") == 0)
			break;
	}
	
	while (!feof(fp))
	{
		static const char l[] = "S: Sysfs=";

		fgets(line, sizeof(line), fp);
		if (strstr(line, l) == line)
		{
			line[strcspn(line, "\n")] = '\0';
			fname = str_concat("/sys", &line[sizeof(l) - 1], "/enable", NULL);
			break;
		}
	}

	fclose(fp);
	return fname;
}

int main(int argc, char **argv)
{
	static const char cal_filename[] = "/sys/devices/virtual/sensors/proximity_sensor/prox_thresh";
	const char *cal_data;
	size_t cal_data_len;
	char *prox_en_filename;
	int ifd;
	int wd;
	char iev_buf[sizeof(struct inotify_event) + NAME_MAX + 1];
	struct inotify_event *iev = (struct inotify_event *)iev_buf;

	if (argc != 2)
	{
		fprintf(stderr, "Usage: %s <cal_data>\n", argc >= 1 ? argv[0] : "msm_sensor_prox_calib");
		exit(1);
	}

	cal_data = argv[1];
	cal_data_len = strlen(cal_data);

	prox_en_filename = get_prox_en_filename();
	if (!prox_en_filename)
		exit(1);

	fprintf(stderr, "%s: Watching writes to \"%s\".\n", argv[0], prox_en_filename);

	ifd = inotify_init1(IN_CLOEXEC);
	if (ifd < 0)
		exit(1);

	wd = inotify_add_watch(ifd, prox_en_filename, IN_MODIFY);
	if (wd < 0)
		exit(1);

	while (1)
	{
		int cfd;
		ssize_t rsize;

		cfd = open(cal_filename, O_WRONLY | O_CLOEXEC);
		write(cfd, cal_data, cal_data_len);
		close(cfd);

		do
		{
			errno = 0;
			rsize = read(ifd, iev_buf, sizeof(iev_buf));
		}
		while (rsize < 0 && errno == EINTR);

		if (rsize < 0)
			exit(1);

		fprintf(stderr, "%s: Got write to \"%s\", recalibrate proximity sensor.\n", argv[0], prox_en_filename);
	}

	free(prox_en_filename);
	close(ifd);
}
